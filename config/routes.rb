Rails.application.routes.draw do
  
  get "/index" => "users#index"
  get "/show:id" => "users#show"
  post "/create" => "users#create"
  delete "/destroy" => "users#destroy"

  get "/index" => "orders#index"
  get "/show:id" => "orders#show"
  post "/create" => "orders#create"
  delete "/destroy" => "orders#destroy"

  get "/index" => "meals#index"
  get "/show:id" => "meals#show"
  post "/create" => "meals#create"
  delete "/destroy" => "meals#destroy"

  get "/index" => "situations#index"
  get "/show:id" => "situations#show"
  post "/create" => "situations#create"
  delete "/destroy" => "situations#destroy"

  get "/index" => "meal_categories#index"
  get "/show:id" => "meal_categories#show"
  post "/create" => "meal_categories#create"
  delete "/destroy" => "meal_categories#destroy"

end
