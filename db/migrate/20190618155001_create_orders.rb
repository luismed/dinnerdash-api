class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :price
      t.references :user_id, foreign_key: true
      t.references :situation_id, foreign_key: true

      t.timestamps
    end
  end
end
