class MealcategoriesController < ApplicationController
    def index
        @meal_categories = Meal_categories.all
        render json: @meal_categories
    end

    def show
        @meal_categories = Meal_categories.find(params[:id])
        render json: @meal_categories
    end

    def create
        @meal_category = Meal_categories.new(name: params[:name])
        if meal_category.save
            render json: @meal_category
        else
            render json: {status: ERRO, erro: "não foi"}
        end
    end

    def destroy
        @meal_category = Meal_categories.find(params[:id])
        @meal_category.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
