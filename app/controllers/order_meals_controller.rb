class OrderMealsController < ApplicationController
    def index
        @order_meals = Order_meal.all
        render json: @order_meals
    end

    def show
        @order_meal = Order_meal.find(params[:id])
        render json: @order_meal
    end

    def create
        @order_meal = Order_meal.new(name: params[:name])
        if @order_meal.save
            render json: @order_meal
        else
            render json: {status: ERRO, erro: "não foi"}
        end
    end

    def destroy
        @order_meal = Order_meal.find(params[:id])
        @order_meal.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
