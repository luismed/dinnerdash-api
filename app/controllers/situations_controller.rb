class SituationsController < ApplicationController
    def index
        @situations = Situation.all
        render json: @situations
    end

    def show
        @situations = Situation.find(params[:id])
        render json: @situations
    end

    def create
        @situation = Situation.new(description: params[:description])
        if user.save
            render json: @situation
        else
            render json: {status: ERRO, erro: "não foi"}
        end
    end

    def destroy
        @situation= Situation.find(params[:id])
        @situation.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
