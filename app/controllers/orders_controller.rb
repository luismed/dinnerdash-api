class OrdersController < ApplicationController
    def index
        @orders = Order.all
        render json: @orders
    end

    def show
        @order = Order.find(params[:id])
        render json: @order
    end

    def create
        @order = Order.new(price: params[:price])
        if order.save
            render json: @order
        else
            render json: {status: ERRO, erro: "não foi"}
        end
    end

    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
