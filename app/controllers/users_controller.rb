class UsersController < ApplicationController
    def index
        @users = User.all
        render json: @users
    end

    def show
        @users = User.find(params[:id])
        render json: @users
    end

    def create
        @user = User.create(name: params[:name],email: params[:email],password: params[:passaword],admin: params[:admin])
        if @user.save
            render json: @user 
        else
            render json: {erro: "Falha Critica"}
        end
    end

    def destroy
        @user = User.find(params[:id])
        @user.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
