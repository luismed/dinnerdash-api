class MealsController < ApplicationController
    def index
        @meals = Meal.all
        render json: @meals
    end

    def show
        @meals = Meal.find(params[:id])
        render json: @meals
    end

    def create
        @meal = Meal.new(name: params[:name],description: params[:description],price: params[:price],available: params[:avaliable])
        if meal.save
            render json: @meals
        else
            render json: {status: ERRO, erro: "não foi"}
        end
    end

    def destroy
        @meal = Meal.find(params[:id])
        @meal.destroy
        render json: {status: SUCCESS, message: "Destruido com sucesso"}
    end
end
