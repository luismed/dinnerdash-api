class Meal < ApplicationRecord
    has_many :order_meals
    belongs_to :mealcategory
end
